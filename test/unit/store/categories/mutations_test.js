import chai from 'chai'
import * as mutationTypes from '../../../../src/store/categories/mutation_types'
import mutations from '../../../../src/store/categories/mutations';

var expect = chai.expect

let categoryReceivedObject = {
  labels: {
    fr: 'Categorie',
    en: 'Category'
  },
  tags: ['term1','term2'],
  links: [
    { rel: "self", href:"http://localhost/categories/1"},
    { rel: "thumbnail", href:"http://localhost/categories/1/thumbnail"}
  ]
}

let categoryMappedObject = {
  labels: {
    fr: 'Categorie',
    en: 'Category'
  },
  tags: ['term1','term2'],
  detailUrl: "http://localhost/categories/1",
  thumbnailUrl: "http://localhost/categories/1/thumbnail"
}

describe('Categories', function() {
  describe('Mutations', function() {
    describe('#listed_terms()', function() {
      it('should set the terms property of the state', function() {
        let terms = [
          {
            term: 'term1',
            custom: true,
            links: [{
              rel: 'self', href: 'http://localhost/terms/1'
            }]
          }
        ]

        let state = {}

        let payload = {
          type: mutationTypes.LISTED_TERMS,
          terms: terms
        }
        // when
        mutations.listed_terms(state, payload)

        // then
        expect(state.terms).to.eql([
          {
            term: 'term1',
            custom: true,
            detailUrl: 'http://localhost/terms/1'
          }
        ])

      })
    }),
    describe('#listed_categories()', function() {
      it('should set the categories property of the state', function() {
        // given
        let categories = [categoryReceivedObject]

        let payload = {
          type: mutationTypes.LISTED_CATEGORIES,
          categories: categories
        }
        let state = {
          categories: []
        }
        // when
        mutations.listed_categories(state, payload)

        // then
        expect(state.categories).to.eql([categoryMappedObject])
      })
    })

    describe('#category_created()', function() {
      it('should add the category to the categories property of the state', function() {
        // given
        let category = categoryReceivedObject

        let payload = {
          type: mutationTypes.CATEGORY_TO_EDIT_LOADED,
          category: category
        }

        let state = {
          edited_category: {}
        }
        // when
        mutations.category_to_edit_loaded(state, payload)

        // then
        expect(state.edited_category).to.eql(categoryMappedObject)
      })
    })

    describe('#category_updated()', function() {
      it('should update the category to the specified index in the categories property of the state', function() {
        // given
        let category = categoryReceivedObject

        let payload = {
          type: mutationTypes.CATEGORY_UPDATED,
          categoryIndex: 0,
          category: category
        }

        let state = {
          categories: [{}]
        }
        // when
        mutations.category_updated(state, payload)

        // then
        expect(state.categories).to.eql([categoryMappedObject])
      })
    })
  })
})

