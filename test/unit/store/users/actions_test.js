import chai from 'chai'
import 'mocha-sinon';
import sinonChai from "sinon-chai";

import BackendApi from '../../../../src/api/backend_api'
import * as userActions from '../../../../src/store/users/actions'
import * as userMutationTypes from '../../../../src/store/users/mutation_types'
import injector from 'vue-inject';
import { beforeEach } from 'mocha'

chai.use(sinonChai)
var expect = chai.expect

let userWithoutPassword = {
  username: "jdoe",
  admin: true,
  name: "John doe"
}

let userWithPassword = {
  username: "jdoe",
  name: "John doe",
  admin: true,
  password: "1234"
}

let userWithoutUsername = {
  name: "John doe",
  admin: true,
  password: "1234"
}

let userWithoutUsernameOrPassword = {
  name: "John doe",
  admin: true
}

describe('Users', function() {
  describe('Actions', function() {
    var backendMock

    beforeEach(function() {
      backendMock = this.sinon.createStubInstance(BackendApi)
      injector.constant('backendApi', backendMock)
    });
    
    describe('#listUsers', function() {

      it('should call the listUsers method on the backendApi', async function() {
        // given
        backendMock.listUsers.resolves([userWithoutPassword])
        let commit = this.sinon.stub();

        // when
        let resp = await userActions.listUsers({commit})

        // then
        expect(commit).to.have.been.calledWith(userMutationTypes.LISTED_USERS, {users:[userWithoutPassword]})
        expect(backendMock.listUsers).to.have.been.calledWith()
      })
    });

    describe('#saveUser', function() {
      it('should call the save method from the backend with the user data', async function() {
        // given
        backendMock.saveUser.resolves(userWithoutPassword)

        let commit = this.sinon.stub()

        // when
        await userActions.saveUser({commit}, userWithPassword)

        // then
        expect(commit).to.have.been.calledWith(userMutationTypes.USER_ADDED, {
          user: userWithoutPassword
        })

        expect(backendMock.saveUser).to.have.been.calledWith("jdoe", userWithoutUsername)
      });

      it('should be able to call the save method without a password for an existing user', async function() {
        // given
        backendMock.saveUser.resolves(userWithoutPassword)

        let commit = this.sinon.stub()

        // when
        await userActions.saveUser({commit}, userWithoutPassword)

        // then
        expect(commit).to.have.been.calledWith(userMutationTypes.USER_ADDED, {
          user: userWithoutPassword
        })

        expect(backendMock.saveUser).to.have.been.calledWith("jdoe", userWithoutUsernameOrPassword)
      });
    });

  });
});