import chai from 'chai'
import * as mutationTypes from '../../../../src/store/home/mutation_types'
import mutations from '../../../../src/store/home/mutations';

var expect = chai.expect

describe('Home', function() {
  describe('Mutations', function() {
    describe('#listed_endpoints()', function() {
      it('should set the endpoint property of the state', function() {
        // given
        let urls = [
          {picture: "http://app2/picture", name: "app2", url: "http://localhost:10002"}
        ]

        let payload = {
          type: mutationTypes.LISTED_ENDPOINTS,
          endpoints: urls
        }
        let state = {
          endpoints: [
            {picture: "http://app1/picture", name: "app1", url: "http://localhost:10001"}
          ]
        }
        // when
        mutations.listed_endpoints(state, payload)

        // then
        expect(state.endpoints).to.eql([
          {picture: "http://app1/picture", name: "app1", url: "http://localhost:10001"},
          {picture: "http://app2/picture", name: "app2", url: "http://localhost:10002"}
        ])
      })
    })

    describe('#reloading_endpoints()', function() {
      it('should set the endpoint property of the state', function() {
        // given
        let payload = {
          type: mutationTypes.RELOADING_ENDPOINTS
        }

        let state = {
          endpoints: [
            {picture: "http://app1/picture", name: "app1", url: "http://localhost:10001"}
          ]
        }
        // when
        mutations.reloading_endpoints(state, payload)

        // then
        expect(state.endpoints).to.eql([])
      })
    })

    describe('#listed_category_endpoints()', function() {
      it('should set the endpoint property of the state', function() {
        // given
        let urls = [
          {picture: "http://app2/picture", name: "app2", url: "http://localhost:10002"}
        ]
        let payload = {
          type: mutationTypes.LISTED_ENDPOINTS,
          endpoints: urls
        }
        let state = {
          categoryEndpoints: [
            {picture: "http://app1/picture", name: "app1", url: "http://localhost:10001"}
          ]
        }
        // when
        mutations.listed_category_endpoints(state, payload)

        // then
        expect(state.categoryEndpoints).to.eql([
          {picture: "http://app1/picture", name: "app1", url: "http://localhost:10001"},
          {picture: "http://app2/picture", name: "app2", url: "http://localhost:10002"}
        ])
      })
    })

    describe('#reloading_endpoints()', function() {
      it('should set the endpoint property of the state', function() {
        // given
        let payload = {
          type: mutationTypes.RELOADING_ENDPOINTS
        }

        let state = {
          categoryEndpoints: [
            {picture: "http://app1/picture", name: "app1", url: "http://localhost:10001"}
          ]
        }
        // when
        mutations.reloading_category_endpoints(state, payload)

        // then
        expect(state.categoryEndpoints).to.eql([])
      })
    })
  })
})
