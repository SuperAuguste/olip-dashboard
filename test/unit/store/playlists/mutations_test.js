import chai from 'chai'
import * as mutationTypes from '../../../../src/store/playlists/mutation_types'
import mutations from '../../../../src/store/playlists/mutations';

var expect = chai.expect

let playlistReceivedObject = {
  "id": 4,
  "title": "test playlist",
  "links": [
      {
          "rel": "self",
          "href": "http://192.168.6.172:5002/playlists/4"
      }
  ],
  "user": {
      "url": "http://localhost/users/jdoe",
      "name": "John Doe"
  },
  "pinned": false,
  "applications": [],
  "contents": []
}

let mappedPlaylist = {
  "id": 4,
  "title": "test playlist",
  "detailUrl": "http://192.168.6.172:5002/playlists/4",
  "user": {
    "name": "John Doe",
    "url": "http://localhost/users/jdoe"
  },
  "pinned": false,
  "applications": [],
  "contents": [],
  "thumbnailUrl": "http://192.168.6.172:5002/playlists/4/thumbnail"
}

describe('Playlists', function() {
  describe('Mutations', function() {
    describe('#playlist_to_edit_loaded()', function() {
      it('should set the edited_playlist property of the state', function() {
        let payload = {
          type: mutationTypes.PLAYLIST_TO_EDIT_LOADED,
          playlist: playlistReceivedObject
        }
        let state = {}
        // when
        mutations.playlist_to_edit_loaded(state, payload)

        // then
        expect(state.edited_playlist).to.eql(mappedPlaylist)
      })
    })
  })
})

