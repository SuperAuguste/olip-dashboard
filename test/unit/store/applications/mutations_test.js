import chai from 'chai'
import * as mutationTypes from '../../../../src/store/applications/mutation_types'
import mutations from '../../../../src/store/applications/mutations';

var expect = chai.expect

describe('Applications', function() {
  describe('Mutations', function() {
    describe('#listed_applications()', function() {
      it('should set the applications property of the state', function() {
        // given
        let applications = [{
          name: "test", 
          bundle: "test.app",
          current_state: "downloaded",
          target_state: "installed",
          repository_version: "3.0.0",
          current_version: "2.0.0",
          has_content_to_upgrade: true,
          target_version: "1.0.0",
          weight:12,
          visible: false,
          links: [
            { rel: 'self', href: "http://localhost:8084/applications/test.app"},
            { rel: 'picture', href: "http://localhost:8084/applications/test.app/picture"},
            { rel: 'target-state', href: "http://localhost:8084/applications/test.app/target-state"},
            { rel: 'display-settings', href: "http://localhost:8084/applications/test.app/display-settings"}
          ]
        }]

        let payload = {
          type: mutationTypes.LISTED_APPLICATIONS,
          applications: applications
        }
        let state = {}
        // when
        mutations.listed_applications(state, payload)
        // then
        expect(state.applications).to.eql([{
          name: "test", 
          bundle: "test.app",
          currentState: "downloaded",
          targetState: "installed",
          repositoryVersion: "3.0.0",
          currentVersion: "2.0.0",
          targetVersion: "1.0.0",
          hasContentToUpgrade: true,
          visible: false,
          weight: 12,
          targetStateUrl: "http://localhost:8084/applications/test.app/target-state",
          detail: "http://localhost:8084/applications/test.app",
          picture: "http://localhost:8084/applications/test.app/picture",
          displaySettingsUrl: "http://localhost:8084/applications/test.app/display-settings"
        }])
      })
    })

    describe('#selected_applications()', function() {
      it('should set the selected_application property of the state', function() {
        // given
        let application = { name:"test", 
          bundle: "test.app", 
          description: "des",
          repository_version: "3.0.0",
          current_version: "2.0.0",
          target_version: "1.0.0",
          current_state: "uninstalled",
          links: [
            { rel: 'picture', href: "http://localhost:8084/applications/test.app/picture"},
            { rel: 'target-state', href: "http://localhost:8084/applications/test.app/target-state"}
          ]
        }

        let target_state = {
          target_state: "installed"
        }

        let payload = {
          types: mutationTypes.SELECTED_APPLICATIONS,
          application: application,
          targetState: target_state
        }
        let state = {}

        // when
        mutations.selected_application(state, payload)

        // then
        expect(state.selected_application).to.eql({
          name: "test", 
          bundle: "test.app",
          description: "des",
          currentState: "uninstalled",
          repositoryVersion: "3.0.0",
          currentVersion: "2.0.0",
          targetVersion: "1.0.0",
          picture: "http://localhost:8084/applications/test.app/picture",
          targetStateUrl: "http://localhost:8084/applications/test.app/target-state",
          targetState: "installed"
        })

      })
      
    })

    describe('#download_started()', function() {
      it('should set the target_state property of the selected_application', function() {
        // given
        let payload = {
          bundle: "test.app"
        }

        let state = { selected_application:{
          'bundle': 'test.app',
          'targetState': 'uninstalled'
        }}

        // when
        mutations.download_started(state, payload)

        // then
        expect(state.selected_application).to.eql({
          'bundle': 'test.app',
          'targetState': 'downloaded'
        })

      })
    })

    describe('#listed_downloaded_applications()', function() {
      it('should set the downloaded_applications property of the state', function() {
        // given
        let applications = [{
          name: "test", 
          bundle: "test.app",
          current_state: "downloaded",
          target_state: "installed",
          repository_version: "3.0.0",
          current_version: "2.0.0",
          target_version: "1.0.0",
          visible: false,
          has_content_to_upgrade: true,
          visible: false,
          links: [
            { rel: 'self', href: "http://localhost:8084/applications/test.app"},
            { rel: 'picture', href: "http://localhost:8084/applications/test.app/picture"},
            { rel: 'target-state', href: "http://localhost:8084/applications/test.app/target-state"},
            { rel: 'display-settings', href: "http://localhost:8084/applications/test.app/display-settings"}
          ]
        }]

        let payload = {
          type: mutationTypes.LISTED_DOWNLOADED_APPLICATIONS,
          applications: applications
        }
        let state = {}
        // when
        mutations.listed_downloaded_applications(state, payload)

        // then
        expect(state.downloaded_applications).to.eql([{
          name: "test", 
          bundle: "test.app",
          currentState: "downloaded",
          targetState: "installed",
          repositoryVersion: "3.0.0",
          currentVersion: "2.0.0",
          targetVersion: "1.0.0",
          hasContentToUpgrade: true,
          visible:false,
          targetStateUrl: "http://localhost:8084/applications/test.app/target-state",
          detail: "http://localhost:8084/applications/test.app",
          picture: "http://localhost:8084/applications/test.app/picture",
          displaySettingsUrl: "http://localhost:8084/applications/test.app/display-settings",
        }])
      })
    })

    describe('#listed_installed_applications()', function() {
      it('should set the installed_applications property of the state', function() {
        // given
        let applications = [{
          name:"test", 
          bundle:"test.app",
          current_state: "installed",
          target_state: "downloaded",
          repository_version: "3.0.0",
          current_version: "2.0.0",
          has_content_to_upgrade: true,
          visible: false,
          weight: 12,
          target_version: "1.0.0",
          links: [
            { rel: 'self', href: "http://localhost:8084/applications/test.app"},
            { rel: 'picture', href: "http://localhost:8084/applications/test.app/picture"},
            { rel: 'target-state', href: "http://localhost:8084/applications/test.app/target-state"},
            { rel: 'display-settings', href: "http://localhost:8084/applications/test.app/display-settings"}
          ]
        }]

        let payload = {
          type: mutationTypes.LISTED_INSTALLED_APPLICATIONS,
          applications: applications
        }
        let state = {}
        // when
        mutations.listed_installed_applications(state, payload)

        // then
        expect(state.installed_applications).to.eql([{
          name: "test", 
          bundle: "test.app",
          currentState: "installed",
          targetState: "downloaded",
          repositoryVersion: "3.0.0",
          currentVersion: "2.0.0",
          targetVersion: "1.0.0",
          visible: false,
          weight:12,
          hasContentToUpgrade: true,
          targetStateUrl: "http://localhost:8084/applications/test.app/target-state",
          detail: "http://localhost:8084/applications/test.app",
          picture: "http://localhost:8084/applications/test.app/picture",
          displaySettingsUrl: "http://localhost:8084/applications/test.app/display-settings",
        }])
      })

      it('should include a contentsLink property in the state if received from the API', function() {
        // given
        let applications = [{
          name:"test", 
          bundle:"test.app",
          current_state: "installed",
          target_state: "downloaded",
          links: [
            { rel: 'self', href: "http://localhost:8084/applications/test.app"},
            { rel: 'picture', href: "http://localhost:8084/applications/test.app/picture"},
            { rel: 'target-state', href: "http://localhost:8084/applications/test.app/target-state"},
            { rel: 'contents', href: "http://localhost:8084/applications/test.app/contents"},
            { rel: 'display-settings', href: "http://localhost:8084/applications/test.app/display-settings"}
          ]
        }]

        let payload = {
          type: mutationTypes.LISTED_INSTALLED_APPLICATIONS,
          applications: applications
        }
        let state = {}
        // when
        mutations.listed_installed_applications(state, payload)

        // then
        expect(state.installed_applications[0].contentsLink).to.equal("http://localhost:8084/applications/test.app/contents")


      })
    })

    describe('#target_state_changed()', function() {
      it('should set the target_state of the downloaded application', function() {
        // given
        let downloaded_applications = [{
          name:"test", 
          bundle:"test.app",
          currentState: "downloaded",
          targetState: "downloaded"
        }]

        let state = {downloaded_applications}

        // when
        mutations.target_state_changed(state, {bundle: "test.app", targetState: "installed"});

        // then
        expect(state.downloaded_applications).to.eql([{
          name:"test", 
          bundle:"test.app",
          currentState: "downloaded",
          targetState: "installed"
        }])

        // object must be a copy
        expect(Object.is(state.downloaded_applications, downloaded_applications)).to.be.false
        

      })

      it('should set the target_state of the installed application if app is in the installed state', function() {
        // given
        let installed_applications = [{
          name:"test", 
          bundle:"test.app",
          currentState: "installed",
          targetState: "installed"
        }]

        let state = {installed_applications}

        // when
        mutations.target_state_changed(state, {bundle: "test.app", targetState: "downloaded"});

        // then
        expect(state.installed_applications).to.eql([{
          name:"test", 
          bundle:"test.app",
          currentState: "installed",
          targetState: "downloaded"
        }])

        // object must be a copy
        expect(Object.is(state.installed_applications, installed_applications)).to.be.false
        

      })
      
    })

    describe('#upgrade_started()', function() {
      it('should set the target_version of the installed application', function() {
        // given
        let installed_applications = [{
          name:"test", 
          bundle:"test.app",
          currentState: "installed",
          targetState: "installed",
          targetVersion: "1.0.0"
        }]

        let state = {installed_applications}

        // when
        mutations.upgrade_started(state, {bundle: "test.app", targetVersion: "2.0.0"});

        // then
        expect(state.installed_applications).to.eql([{
          name:"test", 
          bundle:"test.app",
          currentState: "installed",
          targetState: "installed",
          targetVersion: "2.0.0"
        }])

        // object must be a copy
        expect(Object.is(state.installed_applications, installed_applications)).to.be.false
        

      })
      
    })
  })

  describe('#listed_contents_for_app()', function() {
    it('should set the the contents property of the corresponding installed_app', function() {
      // given
      let installed_applications = [{
        name:"test", 
        bundle:"test.app"
      }]

      let state = {installed_applications}
      let contents = [
        {content_id: "c1", name: "Content", description: "desc", language: "en", subject: "subject",
         current_state:'uninstalled', target_state:'uninstalled', repository_version: '3.0.0',
         target_version: '2.0.0', current_version: '1.0.0', size: 1,
        links:[
         { rel: 'target-state', href: 'http://localhost:8084/applications/test.app/contents/c1/target-state'} 
        ]}
      ]
      // when
      mutations.listed_contents_for_app(state, {bundle: "test.app", contents: contents});

      // then
      expect(state.installed_applications).to.eql([{
        name:"test", 
        bundle:"test.app",
        contents: [
          {content_id: "c1",
          name: "Content",
          description: "desc",
          language: "en",
          subject: "subject",
          size: 1,
          repositoryVersion: '3.0.0',
          targetVersion: '2.0.0',
          currentVersion: '1.0.0',
          currentState: 'uninstalled',
          targetState: 'uninstalled',
          targetStateUrl: 'http://localhost:8084/applications/test.app/contents/c1/target-state'}
        ]
      }])

      // object must be a copy
      expect(Object.is(state.installed_applications, installed_applications)).to.be.false
      

    })
  })

  describe('#content_installation_started()', function() {
    it('should set the target_state property for the downloaded content to installed', function() {
      // given
      let installed_applications = [{
        name: "test", 
        bundle: "test.app",
        contents:[
          {content_id: "c1", name: "Content", description: "desc", currentState: "uninstalled", targetState: "downloaded"}
        ]
      }]

      let state = {installed_applications}
      let cId = 'c1'

      // when
      mutations.content_installation_started(state, {bundle: "test.app", cId, newState: 'installed'});

      // then
      expect(state.installed_applications).to.eql([{
        name: "test", 
        bundle: "test.app",
        contents: [
          {content_id: "c1", name: "Content", description: "desc", currentState: "uninstalled", targetState: "installed"}
        ]
      }])

      // object must be a copy
      expect(Object.is(state.installed_applications, installed_applications)).to.be.false
      

    })

    
  })

  describe('#content_upgraded()', function() {
    it('should set the targetVersion of the content to the repositoryVersion', function() {
      // given
      let installed_applications = [{
        name: "test", 
        bundle: "test.app",
        contents:[
          {content_id: "c1", name: "Content", description: "desc", repositoryVersion: "1.0.0", targetVersion: "0.0.1"}
        ]
      }]

      let state = {installed_applications}
      let cId = 'c1'

      // when
      mutations.content_upgraded(state, {bundle: "test.app", cId, targetVersion: '1.0.0'});

      // then
      expect(state.installed_applications).to.eql([{
        name: "test", 
        bundle: "test.app",
        contents: [
          {content_id: "c1", name: "Content", description: "desc", repositoryVersion: "1.0.0", targetVersion: "1.0.0"}
        ]
      }])

      // object must be a copy
      expect(Object.is(state.installed_applications, installed_applications)).to.be.false
      

    })

    
  })

  describe('#target_state_changed()', function() {
    it('should set the fields of the installed application', function() {
      // given
      let installed_applications = [{
        name:"test", 
        bundle:"test.app",
        visible: true
      }]

      let state = {installed_applications}

      // when
      mutations.installed_application_changed(state, {bundle: "test.app", visible: false});

      // then
      expect(state.installed_applications).to.eql([{
        name:"test", 
        bundle:"test.app",
        visible: false
      }])

      // object must be a copy
      expect(Object.is(state.installed_applications, installed_applications)).to.be.false
      

    })

    
  })

  describe('#replaced_installed_applications()', function() {
    it('should replace the whole installed_applications array', function() {
      // given
      let installed_applications = [{
        name:"test", 
        bundle:"test.app",
        visible: true,
        weight: 12
      }]

      let state = {installed_applications}

      let newApplications = [{
        name:"test2", 
        bundle:"test.app.2",
        visible: false,
        weight: 2
      }]
      // when
      mutations.replaced_installed_applications(state, {bundle: "test.app", applications: newApplications});

      // then
      expect(state.installed_applications).to.eql(newApplications)

      // object must be a copy
      expect(Object.is(state.installed_applications, installed_applications)).to.be.false

    })

    
  })
})

