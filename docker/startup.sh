#!/bin/sh

cat >/usr/share/nginx/html/config.json <<EOF
{
  "api_url": "$API_URL"
}
EOF

exec "$@"

