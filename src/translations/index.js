import ar from './ar'
import fr from './fr'
import enUS from './en-us'
import bn from './bn'
import my from './my'
import rhg from './rhg'
import es from './es'
import sw from './sw'
import am from './am'
import el from './el'
import hi from './hi'
import run from './run'
import ur from './ur'
import fa from './fa'
import ps from './ps'
export default {
  ar,
  fr,
  bn,
  my,
  rhg,
  es,
  sw,
  am,
  el,
  hi,
  run,
  ur,
  fa,
  ps,
  'en-us': enUS
}
