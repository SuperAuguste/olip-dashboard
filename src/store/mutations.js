import * as mutationTypes from './mutation_types'

let listedSearchResultsMutation = mutationTypes.LISTED_SEARCH_RESULTS

export default {
  [listedSearchResultsMutation]: (state, {results}) => {
    state.searchResults = results
  }
}
