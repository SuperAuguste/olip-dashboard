import Vue from 'vue'
import Vuex from 'vuex'

import { vuexOidcCreateStoreModule } from 'vuex-oidc'

import getOidcSettings from './oidc-settings'

import applications from './applications'
import home from './home'
import users from './users'
import categories from './categories'
import playlists from './playlists'

import state from './state'
import mutations from './mutations'
import * as actions from './actions'
import router from '../router'

// Quasard initialize the router automatically. We therefore need to create a first store object
// then we will register the oidc module when requested by the router, so that we can retrieve
// oidc settings asynchronously via the config

Vue.use(Vuex)

let store = new Vuex.Store({
  modules: {
    applications,
    home,
    users,
    categories,
    playlists
  },
  state,
  mutations,
  actions
})

let initiated = false

export const createStore = async () => {
  if (!initiated) {
    let oidcSettings = await getOidcSettings()

    if (!initiated) { // may happend if several routes are called
      store.registerModule('oidc', vuexOidcCreateStoreModule(oidcSettings, {
        accessTokenExpired: () => {
          router.push('/home')
        }
      }))

      initiated = true
    }
  }

  return store
}

export default store
