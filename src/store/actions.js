import injector from 'vue-inject'
import * as mutationTypes from './mutation_types'

let encase = injector.encase.bind(injector)

export const search = encase(['backendApi'], (backendApi) => async ({commit}, term) => {
  let searched = encodeURI(term)

  let xml = await backendApi.search(searched)

  let xpathResult = xml.evaluate('//a:entry', xml, () => 'http://www.w3.org/2005/Atom', XPathResult.ANY_TYPE)

  let allResults = []

  var thisNode = xpathResult.iterateNext()

  while (thisNode) {
    let searchResult = {}

    for (var c in thisNode.children) {
      let e = thisNode.children[c]

      if (e.tagName === 'title') {
        searchResult.title = e.textContent
      } else if (e.tagName === 'link') {
        searchResult.link = e.attributes['href'].value
      } else if (e.tagName === 'summary') {
        searchResult.summary = e.textContent
      }
    }

    thisNode = xpathResult.iterateNext()
    allResults.push(searchResult)

    commit(mutationTypes.LISTED_SEARCH_RESULTS, {results: allResults})
  }
})
