import injector from 'vue-inject'
import * as mutationTypes from './mutation_types'
import _ from 'lodash'
import { Notify } from 'quasar'
let encase = injector.encase.bind(injector)

export const listApps = encase(['backendApi'], (backendApi) => async ({commit}) => {
  let applications = await backendApi.listApplications()

  commit(mutationTypes.LISTED_APPLICATIONS, {applications: applications})
})

export const selectApp = encase(['backendApi'], (backendApi) => async ({commit, state}, bundle) => {
  let app = _.find(state.applications, a => a.bundle === bundle)
  let appDetail = await backendApi.getAppDetail(app.detail)

  let targetStateUrl = _.find(appDetail.links, l => l.rel === 'target-state').href

  let targetState = await backendApi.getAppTargetState(targetStateUrl)

  commit(mutationTypes.SELECTED_APPLICATION, {application: appDetail, targetState: targetState})
})

export const downloadApp = encase(['backendApi'], (backendApi) => async ({commit, state}, bundle) => {
  let app = _.find(state.applications, a => a.bundle === bundle)

  await backendApi.setAppTargetState(app.targetStateUrl, 'downloaded')

  commit(mutationTypes.DOWNLOAD_STARTED, {bundle: bundle})
})

export const listInstalledApps = encase(['backendApi'], (backendApi) => async ({commit}) => {
  let installedApplications = await backendApi.listApplications('installed')

  commit(mutationTypes.LISTED_INSTALLED_APPLICATIONS, {applications: installedApplications})
})

export const listDownloadedApps = encase(['backendApi'], (backendApi) => async ({commit}) => {
  let downloadedApplications = await backendApi.listApplications('downloaded')

  commit(mutationTypes.LISTED_DOWNLOADED_APPLICATIONS, {applications: downloadedApplications})
})

export const triggerAppInstallation = encase(['backendApi'], (backendApi) => async ({commit, state}, bundle) => {
  let app = _.find(state.downloaded_applications, a => a.bundle === bundle)

  await backendApi.setAppTargetState(app.targetStateUrl, 'installed')

  commit(mutationTypes.TARGET_STATE_CHANGED, {bundle: bundle, target_state: 'installed'})
})

export const triggerAppUpgrade = encase(['backendApi'], (backendApi) => async ({commit, state}, bundle) => {
  let app = _.find(state.installed_applications, a => a.bundle === bundle)

  await backendApi.setAppTargetState(app.targetStateUrl, 'installed', app.repositoryVersion)

  commit(mutationTypes.UPGRADE_STARTED, {bundle: bundle, targetVersion: app.repositoryVersion})
})

export const triggerAppUninstall = encase(['backendApi'], (backendApi) => async ({commit, state}, bundle) => {
  let app = _.find(state.installed_applications, a => a.bundle === bundle)

  await backendApi.setAppTargetState(app.targetStateUrl, 'downloaded', app.repositoryVersion)

  commit(mutationTypes.TARGET_STATE_CHANGED, {bundle: bundle, target_state: 'downloaded'})
})

export const triggerAppDeletion = encase(['backendApi'], (backendApi) => async ({commit, state}, bundle) => {
  let app = _.find(state.downloaded_applications, a => a.bundle === bundle)

  await backendApi.setAppTargetState(app.targetStateUrl, 'uninstalled', app.repositoryVersion)

  Notify.create({
    message: 'Danger, Will Robinson! Danger!'
  })
  commit(mutationTypes.TARGET_STATE_CHANGED, {bundle: bundle, target_state: 'uninstalled'})
})

export const listContentForBundle = encase(['backendApi'], (backendApi) => async ({commit, state}, bundle) => {
  let app = _.find(state.installed_applications, a => a.bundle === bundle)

  if (app) { // we could have no app if we trigger the view without passing by the list of installed applications
    let contentsLink = app.contentsLink

    if (contentsLink) {
      let content = await backendApi.getAppContent(contentsLink)

      commit(mutationTypes.LISTED_CONTENTS_FOR_APP, {bundle: bundle, contents: content})
    }
  }
})

export const triggerContentsTargetStateChange = encase(['backendApi'], (backendApi) => async ({commit, state}, {bundle, contentIds, newState}) => {
  let app = _.find(state.installed_applications, a => a.bundle === bundle)

  contentIds.forEach(async cId => {
    let content = _.find(app.contents, c => c.content_id === cId)

    let targetStateUrl = content.targetStateUrl

    let res = await backendApi.setContentTargetState(targetStateUrl, newState)

    if (typeof res.data.message !== 'undefined') {
      Notify.create({
        message: res.data.message
      })
    } else {
      commit(mutationTypes.CONTENT_INSTALLATION_STARTED, {bundle, cId, newState: newState})
    }
  })
})

export const triggerContentUpgrade = encase(['backendApi'], (backendApi) => async ({commit, state}, {bundle, contentIds}) => {
  let app = _.find(state.installed_applications, a => a.bundle === bundle)

  contentIds.forEach(async cId => {
    let content = _.find(app.contents, c => c.content_id === cId)

    let targetStateUrl = content.targetStateUrl

    let currentState = content.targetState

    await backendApi.setContentTargetState(targetStateUrl, currentState, content.repositoryVersion)

    commit(mutationTypes.CONTENT_UPGRADED, {bundle, cId, targetVersion: content.repositoryVersion})
  })
})

export const toggleVisible = encase(['backendApi'], (backendApi) => async ({commit, state}, {bundle, newVal}) => {
  let app = _.find(state.installed_applications, a => a.bundle === bundle)

  let displaySettingsUrl = app.displaySettingsUrl

  let vis = newVal

  await backendApi.setAppDisplaySettings(displaySettingsUrl, vis, app.weight)

  commit(mutationTypes.INSTALLED_APPLICATION_CHANGED, {bundle, visible: vis})
})

function updateAllApps (backendApi, commit, installedAppBis) {
  let promises = []

  var counter = 0
  // reorder everything
  installedAppBis.forEach(async a => {
    let cnt = counter++

    let displaySettingsUrl = a.displaySettingsUrl

    let p = await backendApi.setAppDisplaySettings(displaySettingsUrl, a.visible, cnt)

    a.weight = cnt

    promises.push(p)
  })

  Promise.all(promises).then(() => {
    commit(mutationTypes.REPLACED_INSTALLED_APPLICATIONS, {applications: installedAppBis})
  })
}

export const moveAppUp = encase(['backendApi'], (backendApi) => ({commit, state}, bundle) => {
  let appIndex = _.findIndex(state.installed_applications, a => a.bundle === bundle)

  if (appIndex === 0) {
    return
  }

  let currentApp = state.installed_applications[appIndex]

  let previousApp = state.installed_applications[appIndex - 1]

  // invert position in the array
  var installedAppBis = _.cloneDeep(state.installed_applications)

  installedAppBis[appIndex] = previousApp
  installedAppBis[appIndex - 1] = currentApp

  updateAllApps(backendApi, commit, installedAppBis)
})

export const moveAppDown = encase(['backendApi'], (backendApi) => ({commit, state}, bundle) => {
  let appIndex = _.findIndex(state.installed_applications, a => a.bundle === bundle)

  if (appIndex >= state.installed_applications.length - 1) {
    return
  }

  let currentApp = state.installed_applications[appIndex]

  let nextApp = state.installed_applications[appIndex + 1]

  // invert position in the array
  var installedAppBis = _.cloneDeep(state.installed_applications)

  installedAppBis[appIndex] = nextApp
  installedAppBis[appIndex + 1] = currentApp

  updateAllApps(backendApi, commit, installedAppBis)
})

export const loadAppConfiguration = encase(['backendApi'], (backendApi) => async ({commit, state}, bundle) => {
  let app = _.find(state.downloaded_applications, a => a.bundle === bundle)

  let configuration = await backendApi.getConfiguration(app.configurationUrl)

  commit(mutationTypes.CONFIGURATION_LOADED, {bundle: bundle, configuration: configuration.configuration})
})

export const saveAppConfiguration = encase(['backendApi'], (backendApi) => async ({commit, state}, {bundle, configuration}) => {
  let app = _.find(state.downloaded_applications, a => a.bundle === bundle)
  console.log(configuration)
  await backendApi.putConfiguration(app.configurationUrl, {configuration: configuration})

  // commit(mutationTypes.CONFIGURATION_SAVED, {bundle: bundle, configuration: configuration.configuration})
})

export const getFreeDiskSpace = encase(['backendApi'], (backendApi) => async ({commit, state}) => {
  let sysinfo = await backendApi.getSysinfo()

  state.sysinfo = sysinfo
})
