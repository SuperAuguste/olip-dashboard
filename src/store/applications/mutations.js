import * as mutationTypes from './mutation_types'
import _ from 'lodash'

let listedApplicationsMutation = mutationTypes.LISTED_APPLICATIONS
let selectedApplicationMutation = mutationTypes.SELECTED_APPLICATION
let downloadStarted = mutationTypes.DOWNLOAD_STARTED
let listedInstalledApplicationsMutation = mutationTypes.LISTED_INSTALLED_APPLICATIONS
let listedDownloadedApplicationsMutation = mutationTypes.LISTED_DOWNLOADED_APPLICATIONS
let targetStateChangedMutation = mutationTypes.TARGET_STATE_CHANGED
let upgradeStartedMutation = mutationTypes.UPGRADE_STARTED
let listedContentsForAppMutation = mutationTypes.LISTED_CONTENTS_FOR_APP
let contentInstallationStartedMutation = mutationTypes.CONTENT_INSTALLATION_STARTED
let contentUpgradedMutation = mutationTypes.CONTENT_UPGRADED
let installedApplicationChanged = mutationTypes.INSTALLED_APPLICATION_CHANGED
let replacedInstalledApplications = mutationTypes.REPLACED_INSTALLED_APPLICATIONS
let configurationLoadedMutation = mutationTypes.CONFIGURATION_LOADED
let sysInfoMutation = mutationTypes.SYSINFO

let appMap = (applications) => _.map(applications, a => {
  let obj = {
    name: a.name,
    bundle: a.bundle,
    currentState: a.current_state,
    targetState: a.target_state,
    currentVersion: a.current_version,
    targetVersion: a.target_version,
    repositoryVersion: a.repository_version,
    hasContentToUpgrade: a.has_content_to_upgrade,
    visible: a.visible,
    detail: _.find(a.links, l => l.rel === 'self').href,
    picture: _.find(a.links, l => l.rel === 'picture').href,
    targetStateUrl: _.find(a.links, l => l.rel === 'target-state').href,
    displaySettingsUrl: _.find(a.links, l => l.rel === 'display-settings').href
  }

  let confLnk = _.find(a.links, l => l.rel === 'configuration')

  if (confLnk) {
    obj.configurationUrl = confLnk.href
  }

  if (a.hasOwnProperty('weight')) {
    obj.weight = a.weight
  }

  let contentsLink = _.find(a.links, l => l.rel === 'contents')

  if (contentsLink) {
    obj.contentsLink = contentsLink.href
  }

  return obj
})

export default {
  [listedApplicationsMutation]: (state, {applications}) => {
    state.applications = appMap(applications)
  },

  [selectedApplicationMutation]: (state, {application, targetState}) => {
    state.selected_application = {
      name: application.name,
      bundle: application.bundle,
      description: application.description,
      currentState: application.current_state,
      currentVersion: application.current_version,
      targetVersion: application.target_version,
      repositoryVersion: application.repository_version,
      picture: _.find(application.links, l => l.rel === 'picture').href,
      targetStateUrl: _.find(application.links, l => l.rel === 'target-state').href,
      targetState: targetState.target_state
    }
  },

  [downloadStarted]: (state, {bundle}) => {
    state.selected_application.targetState = 'downloaded'
  },
  [sysInfoMutation]: (state, {sysinfo}) => {
    state.sysinfo = sysinfo
  },
  [listedInstalledApplicationsMutation]: (state, {applications}) => {
    state.installed_applications = appMap(applications)
  },

  [listedDownloadedApplicationsMutation]: (state, {applications}) => {
    state.downloaded_applications = appMap(applications)
  },
  [targetStateChangedMutation]: (state, {bundle, targetState}) => {
    var newObject = _.cloneDeep(state.downloaded_applications)

    var appToUpdate = _.find(newObject, a => a.bundle === bundle)

    if (appToUpdate == null) {
      // maybe in the installed_applications array ?
      newObject = _.cloneDeep(state.installed_applications)

      appToUpdate = _.find(newObject, a => a.bundle === bundle)

      appToUpdate.targetState = targetState

      state.installed_applications = newObject
    } else {
      appToUpdate.targetState = targetState

      state.downloaded_applications = newObject
    }
  },

  [upgradeStartedMutation]: (state, {bundle, targetVersion}) => {
    let newObject = _.cloneDeep(state.installed_applications)

    let appToUpdate = _.find(newObject, a => a.bundle === bundle)

    appToUpdate.targetVersion = targetVersion

    state.installed_applications = newObject
  },

  [listedContentsForAppMutation]: (state, {bundle, contents}) => {
    let newObject = _.cloneDeep(state.installed_applications)

    let appToUpdate = _.find(newObject, a => a.bundle === bundle)

    appToUpdate.contents = _.map(contents, c => {
      let obj = {
        content_id: c.content_id,
        name: c.name,
        description: c.description,
        language: c.language,
        subject: c.subject,
        size: c.size,
        currentState: c.current_state,
        targetState: c.target_state,
        repositoryVersion: c.repository_version,
        currentVersion: c.current_version,
        targetVersion: c.target_version
      }

      let targetStateUrlObj = _.find(c.links, l => l.rel === 'target-state')
      if (targetStateUrlObj) {
        obj.targetStateUrl = targetStateUrlObj.href
      }

      return obj
    })

    state.installed_applications = newObject
  },

  [contentInstallationStartedMutation]: (state, {bundle, cId, newState}) => {
    let newObject = _.cloneDeep(state.installed_applications)

    let appToUpdate = _.find(newObject, a => a.bundle === bundle)

    let contentToUpdate = _.find(appToUpdate.contents, cont => cont.content_id === cId)

    contentToUpdate.targetState = newState

    state.installed_applications = newObject
  },

  [contentUpgradedMutation]: (state, {bundle, cId, targetVersion}) => {
    let newObject = _.cloneDeep(state.installed_applications)

    let appToUpdate = _.find(newObject, a => a.bundle === bundle)

    let contentToUpdate = _.find(appToUpdate.contents, cont => cont.content_id === cId)

    contentToUpdate.targetVersion = targetVersion

    state.installed_applications = newObject
  },

  [installedApplicationChanged]: (state, args) => {
    let bundle = args.bundle

    let newObject = _.cloneDeep(state.installed_applications)

    let appToUpdate = _.find(newObject, a => a.bundle === bundle)

    if (args.hasOwnProperty('visible')) {
      appToUpdate.visible = args.visible
    }

    state.installed_applications = newObject
  },

  [replacedInstalledApplications]: (state, {applications}) => {
    let newObject = _.cloneDeep(applications)

    state.installed_applications = newObject
  },

  [configurationLoadedMutation]: (state, {bundle, configuration}) => {
    console.log('triggered mutation config loaded')
    let newObject = _.cloneDeep(state.downloaded_applications)

    let app = _.find(newObject, a => a.bundle === bundle)

    app.configuration = configuration

    state.downloaded_applications = newObject
  }
}
