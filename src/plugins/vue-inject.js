// main.js
import injector from 'vue-inject'
import axios from 'axios'
import BackendApi from '../api/backend_api'
import config from '../tools/config'
import store from '../store'

// Add a request interceptor
axios.interceptors.request.use(function (config) {
  // Do something before request is sent
  let accessToken = store.getters.oidcAccessToken

  if (accessToken) {
    config.headers['Authorization'] = 'Bearer ' + accessToken
  }
  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})

injector.constant('axios', axios)
injector.constant('config', config)

injector.service('backendApi', ['axios', 'config'], BackendApi)

export default ({ Vue }) => {
  Vue.use(injector)
}
