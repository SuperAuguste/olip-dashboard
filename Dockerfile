ARG ARCH
FROM offlineinternet/node-v12.9-$ARCH:latest as node

FROM offlineinternet/olip-base-$ARCH as builder

COPY --from=node /usr/local/ /usr/local/

WORKDIR /opt/build

COPY . /opt/build

RUN npm install && \
    npm install -g quasar quasar-cli && \
    quasar build

FROM offlineinternet/olip-base-$ARCH

RUN apt update && \
	apt install -y nginx

COPY --from=builder /opt/build/dist/* /usr/share/nginx/html/

COPY docker/nginx.conf /etc/nginx/nginx.conf

ENV API_URL=http://localhost:5002

ADD docker/startup.sh /startup.sh

ENTRYPOINT ["bash", "/startup.sh"]

CMD ["nginx", "-g", "daemon off;"]
